function loadingScreen(){

  // variables
  var loader = document.querySelector('.loader');
  var loaderText = document.querySelectorAll('.loader__text');
  var loaderTextOneLetters = document.querySelectorAll('.loader__text--1 .letter');
  var loaderTextTwoLetters = document.querySelectorAll('.loader__text--2 .letter');
  var map = document.querySelector('.loader__map');
  var bar = document.querySelector('.loader__bar');
  var barLeft = document.querySelector('.loader__bar--left');
  var barCenter = document.querySelector('.loader__bar--center');
  var barRight = document.querySelector('.loader__bar--right');
  var loadingBar = document.querySelector('.loader__loading-bar');
  var skipIntro = document.querySelector('.loader__loading-bar .pre-title');

  // intiating new timeline
  var tl = new TimelineMax();

  // skip the intro button
  skipIntro.onclick = function() {
    tl.play(15);
  }

  // set default values
  tl.set(loaderText, {autoAlpha:1});

  // timeline 
  tl.add(TweenMax.fromTo(map, 4, {opacity:0}, {opacity:1}));

  tl.add(TweenMax.staggerFromTo(loaderTextOneLetters, 0.3, {opacity:0, top:10}, {opacity:1, top:0}, 0.04), 2);
  tl.add(TweenMax.staggerTo(loaderTextOneLetters, 0.3, {opacity:0, top:-10}, 0.02), 8);
  tl.add(TweenMax.staggerFromTo(loaderTextTwoLetters, 0.3, {opacity:0, top:10}, {opacity:1, top:0}, 0.04), 10.5);
  tl.add(TweenMax.staggerTo(loaderTextTwoLetters, 0.3, {opacity:0, top:-10}, 0.02), 14);

  tl.add(TweenLite.to(map, 1, {opacity:0}), 15);
  tl.add(TweenLite.to(loadingBar, 1, {opacity:0}), 15);

  tl.add(TweenLite.to(barRight, 1.3, {y:'-100%', ease:Power3.easeInOut}), 15.5);
  tl.add(TweenLite.to(barCenter, 1.3, {y:'-100%', ease:Power3.easeInOut}), 15.6);
  tl.add(TweenLite.to(barLeft, 1.3, {y:'-100%', ease:Power3.easeInOut}), 15.7);

  tl.add(TweenLite.to(loader, 0.1, {autoAlpha:0}), 17.7);

}
loadingScreen();

// ---------------------------------------- //

function screenOpening(){

  var button = document.querySelectorAll('.button--main');
  var center = document.querySelectorAll('.main__issues-centre');
  var header = document.querySelectorAll('.main__issues-centre .header');
  var mainPage = document.querySelectorAll('.main__page');
  var mainNav = document.querySelector('.main__nav');
  var overlay = document.querySelectorAll('.main__issues-overlay');
  var buttonBack = document.querySelectorAll('.button--back');
  var arrowLeft = document.querySelectorAll('.main__arrow-left');
  var arrowRight = document.querySelectorAll('.main__arrow-right');

  // intiating new timeline
  var tl = new TimelineMax();

  for (var i = 0; i < button.length; i++) {
    button[i].onclick = function(){
      tl.play(0);
      tl.add(TweenLite.to(center, 0.7, {width:'100%', height:'100%', ease:Power3.easeInOut}));
      tl.add(TweenLite.to(mainNav, 0.5, {y:"20px", autoAlpha:0, ease:Power3.easeInOut}), 0);
      tl.add(TweenLite.to(arrowLeft, 0.5, {x:"-20px", autoAlpha:0, ease:Power3.easeInOut}), 0);
      tl.add(TweenLite.to(arrowRight, 0.5, {x:"20px", autoAlpha:0, ease:Power3.easeInOut}), 0);
      tl.add(TweenLite.to(button, 0.5, {top:-20, autoAlpha:0, ease:Power3.easeInOut}), 0);
      tl.add(TweenLite.to(header, 0.5, {y:"-20px", autoAlpha:0, ease:Power3.easeInOut}), 0);
      tl.add(TweenMax.fromTo(buttonBack, 0.5, {autoAlpha:0, y:"20px"}, {y:"0", autoAlpha:1, ease:Power3.easeInOut}), 0);
      tl.add(TweenMax.fromTo(overlay, 0.7, {autoAlpha:0}, {autoAlpha:1, ease:Power3.easeInOut}), 0);
      tl.add(TweenMax.fromTo(mainPage, 0.7, {autoAlpha:0}, {autoAlpha:1, ease:Power3.easeInOut}), 0.2);
    }
  }

  for (var i = 0; i < buttonBack.length; i++) {
    buttonBack[i].onclick = function(){
      tl.reverse();
      $('.main__page').animate({ scrollTop: 0 }, 'slow');
    }
  }
}
screenOpening();

// ---------------------------------------- //

function homeCarousel(){

  var navLink = document.querySelectorAll('.main__nav-link');
  var navOcean = document.querySelector('.main__nav-link--oceans');
  var navAir = document.querySelector('.main__nav-link--air');
  var navDeforest = document.querySelector('.main__nav-link--deforestation');
  var mainWrap = document.querySelectorAll('.main__issues-wrap');
  var header = document.querySelector('.main__issues-centre .header');
  var button = document.querySelector('.main__issues-centre .button');
  var centre = document.querySelectorAll('.main__issues-centre');
  var bg = document.querySelector('.main__issues-bg');

  var arrowRightOcean = document.querySelector('.main__arrow-right--ocean');
  var arrowRightAir = document.querySelector('.main__arrow-right--air');
  var arrowLeftAir = document.querySelector('.main__arrow-left--air');
  var arrowLeftDeforest = document.querySelector('.main__arrow-left--deforest');

  // intiating new timeline
  var tl = new TimelineMax();

  function navOceanActive(){
    $('.main__nav-link').removeClass('active');
    navOcean.classList.add('active');
    tl.add(TweenMax.to(centre, 0.25, {scale:0.9, ease:Power3.easeInOut}));
    tl.add(TweenMax.to(mainWrap, 0.65, {x:"0%", ease:Power3.easeInOut}));
    tl.add(TweenMax.to(centre, 0.25, {scale:1, ease:Power3.easeInOut}));
    setTimeout(function(){
      bg.style.backgroundImage = "url('/assets/images/oceans/hero.jpg')";
    }, 450);
  }

  function navAirActive(){
    $('.main__nav-link').removeClass('active');
    navAir.classList.add('active');
    tl.add(TweenMax.to(centre, 0.25, {scale:0.9, ease:Power3.easeInOut}));
    tl.add(TweenLite.to(mainWrap, 0.65, {x:"-100%", ease:Power3.easeInOut}));
    tl.add(TweenMax.to(centre, 0.25, {scale:1, ease:Power3.easeInOut}));
    setTimeout(function(){
      bg.style.backgroundImage = "url('/assets/images/air/hero.jpg')";
    }, 450);
  }

  function navDeforestActive() {
    $('.main__nav-link').removeClass('active');
    navDeforest.classList.add('active');
    tl.add(TweenMax.to(centre, 0.25, {scale:0.9, ease:Power3.easeInOut}));
    tl.add(TweenLite.to(mainWrap, 0.65, {x:"-200%", ease:Power3.easeInOut}));
    tl.add(TweenMax.to(centre, 0.25, {scale:1, ease:Power3.easeInOut}));
    setTimeout(function(){
      bg.style.backgroundImage = "url('/assets/images/deforestation/hero.jpg')";
    }, 450);
  }

  navOcean.onclick = function() {
    navOceanActive();
  }

  navAir.onclick = function() {
    navAirActive();
  }

  navDeforest.onclick = function() {
    navDeforestActive();
  }

  arrowRightOcean.onclick = function() {
    navAirActive();
  }

  arrowLeftAir.onclick = function() {
    navOceanActive();
  }

  arrowRightAir.onclick = function() {
    navDeforestActive();
  }

  arrowLeftDeforest.onclick = function() {
    navAirActive();
  }
}
homeCarousel();
