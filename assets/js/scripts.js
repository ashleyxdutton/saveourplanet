function aboutModal() {
  const aboutBtn = document.querySelector('.site-header__icon--info');
  const about = document.querySelector('.about');
  const aboutClose = document.querySelector('.about__close');
  const aboutOverlay = document.querySelector('.about__overlay');

  aboutBtn.addEventListener("click", function(e) {
    e.preventDefault();
    about.classList.add('open');
  });

  aboutClose.addEventListener("click", function(e) {
    e.preventDefault();
    about.classList.remove('open');
  });

  aboutOverlay.addEventListener("click", function(e) {
    e.preventDefault();
    about.classList.remove('open');
  });
}
aboutModal();

/*------------------------------------------*/

// Wrap every character in a span to be animated.
function textSplit() {
  $('.loader__text').each(function(){
    $(this).html($(this).text().replace(/./g, "<span class='letter'>$&</span>"));
  });
}
textSplit();

/*------------------------------------------*/

function topBtn() {
  const topBtn = document.querySelectorAll('.button--top');
  const page = document.querySelectorAll('.main__page');

  for (var i = 0; i < topBtn.length; i++) {
    topBtn[i].onclick = function(){
      $('.main__page').animate({ scrollTop: 0 }, 'fast');
    }
  }
}
topBtn();
