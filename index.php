<?php
  include "header.php";
?>

<main class="site-content">

  <section class="loader">
    <div class="loader__bar loader__bar--left"></div>
    <div class="loader__bar loader__bar--center"></div>
    <div class="loader__bar loader__bar--right"></div>
    <img class="loader__map" src="/assets/svgs/map.svg" alt="SVG Illustration of the world map.">
    <h2 class="loader__text loader__text--1">The planet is in decline in the face of climate change, pollution, overdevelopment, and more.</h2>
    <h2 class="loader__text loader__text--2">How can we Save The Planet?</h2>
    <span class="loader__loading-bar">
      <a class="pre-title" href="#">Skip Intro</a>
    </span>
  </section>

  <section class="main">
    <ul class="main__issues-list">

      <li class="main__issues-bg" style="background-image:url('/assets/images/oceans/hero.jpg')"></li>

      <!-- Oceans -->
      <li class="main__issues-item">
        <div class="main__issues-wrap">
          <div class="main__issues-centre" style="background-image:url('/assets/images/oceans/hero.jpg')">
            <div class="main__issues-overlay"></div>
            <h1 class="header fs--header">Oceans</h1>
            <a class="button button--main" href="#"><span>Explore The Threat</span></a>

            <a class="main__arrow-right main__arrow-right--ocean" href="#">
              <?php include "assets/svgs/arrow-right.svg";?>
            </a>

            <a class="main__arrow-left main__arrow-left--ocean" href="#">
              <?php include "assets/svgs/arrow-left.svg";?>
            </a>

            <div class="main__page">

              <div class="main__page-container">

                <a class="button button--back button--small" href="#">Go back</a>

                <div class="main__column-one-text">
                  <h3 class="pre-title fs--16">Plastic in the Ocean</h3>
                  <p class="bodyfont">There is currently an estimated 12.7 million tonnes of plastic ends up in our oceans every single year! The ocean is slowly turning into one large plastic soup, resulting in catastrophic dangers to the planets marine life. Every year, over a thousand marine turtles die after becoming entangled in plastic waste. The plastic also breaks down into small pieces by sunlight, wind and waves and are then consumed by fish, eventually ending up on our plates. If we do not all take action, by the year 2050, there will be more plastic than fish in the world’s oceans.</p>
                </div>

                <img class="main__image main__image--one" src="/assets/images/oceans/oceans-one.jpg" alt="Plastic waste washed up on a beach.">

                <div class="main__column-two-text">
                  <h3 class="pre-title fs--16">Ocean Acidification</h3>
                  <p class="bodyfont">The burning of fossil fuels doesn't just pollute the air, but the oceans too. Out of all man-man carbon emissions, a quarter is absorbed by the ocean, changing the pH level of surface water. The oceans are acidifying at an alarming rate, faster than they have in 300 million years, threatening marine life such as mussels, clams and coral that require calcium carbonate to build their shells. </p>
                </div>

                <img class="main__image main__image--two" src="/assets/images/oceans/oceans-two.jpg" alt="Underwater marine coral.">

                <div class="main__end">
                  <h3 class="pre-title fs--20">What can we do?</h3>
                  <p class="bodyfont fs--18">It is important that we, as individuals, contribute to cleaning up the oceans and preventing any additional damage. So what can we do? By reducing water pollution at home and being mindful about your plastic consumption, you will be helping to reduce the waste entering the ocean. You can also help by supporting environmental advocacy groups, such as NRDC (link), that help to preserve the Earth's oceans.</p>

                  <div class="main__end-buttons">
                    <a class="button button--back" href="#">Go Back</a>
                    <a class="button button--top" href="#">Top</a>
                  </div>
                </div>

              </div> <!-- container -->
            </div> <!-- page -->

          </div>
        </div>

      </li>

      <!-- Air -->
      <li class="main__issues-item">
        <div class="main__issues-wrap">
          <div class="main__issues-centre" style="background-image:url('/assets/images/air/hero.jpg')">
            <div class="main__issues-overlay"></div>
            <h1 class="header fs--header">Air Pollution</h1>
            <a class="button button--main" href="#"><span>Explore The Threat</span></a>

            <a class="main__arrow-right main__arrow-right--air" href="#">
              <?php include "assets/svgs/arrow-right.svg";?>
            </a>

            <a class="main__arrow-left main__arrow-left--air" href="#">
              <?php include "assets/svgs/arrow-left.svg";?>
            </a>

            <div class="main__page main__page">

              <div class="main__page-container">

                <a class="button button--back button--small" href="#">Go back</a>

                <div class="main__column-one-text">
                  <h3 class="pre-title fs--16">Fossil Fuel Emissions</h3>
                  <p class="bodyfont">Cars, factories, power plants and anything with an engine that combusts fossil fuels, like coal or gas, release harmful particles and gases into the atmosphere. In 2016, poor quality outdoor air caused an estimated <a class="link" href="https://www.nationalgeographic.com/environment/global-warming/pollution/" target="_blank">4.2 million premature deaths</a>. Poor air quality containing harmful particles in soot can penetrate and damage the lungs and bloodstream, significantly damaging human health. </p>
                </div>

                <img class="main__image main__image--one" src="/assets/images/air/air-one.jpg" alt="Power station polluting the atmosphere.">

                <div class="main__column-two-text">
                  <h3 class="pre-title fs--16">Greenhouse Gases</h3>
                  <p class="bodyfont">Greenhouse gases trap heat in the Earth's atmosphere, leading to climate change causing sea levels to rise and extreme weather conditions. Burning fossil fuels like gasoline releases carbon dioxide into the atmosphere, the most common of greenhouse gases. In the past 150 years, humans have raised the levels of greenhouse gases higher than in the <a class="link" href="https://www.nationalgeographic.com/environment/global-warming/global-warming-overview/" target="_blank">last 800,000 years.</a></p>
                </div>

                <img class="main__image main__image--two" src="/assets/images/air/air-two.jpg" alt="Air pollution in a street.">

                <div class="main__end">
                  <h3 class="pre-title fs--20">What can we do?</h3>
                  <p class="bodyfont fs--18">Walking, riding a bike or choosing to take public transport are all simple ways in which you can contribute to reducing the emission of greenhouse gases. If driving is a requirement, choosing a car with better miles per gallon or fully electric will also help. The fewer fossil fuels that are burned will help reduce the amount of air pollution.</p>

                  <div class="main__end-buttons">
                    <a class="button button--back" href="#">Go Back</a>
                    <a class="button button--top" href="#">Top</a>
                  </div>
                </div>

              </div> <!-- container -->
            </div> <!-- page -->

          </div>
        </div>

      </li>

      <!-- Deforestation -->
      <li class="main__issues-item">
        <div class="main__issues-wrap">
          <div class="main__issues-centre" style="background-image:url('/assets/images/deforestation/hero.jpg')">
            <div class="main__issues-overlay"></div>
            <h1 class="header fs--header">Deforestation</h1>
            <a class="button button--main" href="#"><span>Explore The Threat</span></a>

            <a class="main__arrow-right main__arrow-right--deforest" href="#">
              <?php include "assets/svgs/arrow-right.svg";?>
            </a>

            <a class="main__arrow-left main__arrow-left--deforest" href="#">
              <?php include "assets/svgs/arrow-left.svg";?>
            </a>

            <div class="main__page main__page">

              <div class="main__page-container">

                <a class="button button--back button--small" href="#">Go back</a>

                <div class="main__column-one-text">
                  <h3 class="pre-title fs--16">Importance of Forests</h3>
                  <p class="bodyfont">Healthy forests are crucial for the sustainability of the planet. Forests help to keep the climate stable by releasing oxygen and absorbing carbon dioxide. Forests also regulate water supplies and provide a home to <a class="link" href="http://wwf.panda.org/our_work/forests/" target="_blank">80% of all species</a> found on land. Despite the importance of forests, 40% of the world's forests have been lost due to human impact. Deforestation is a severe matter and is currently more urgent than ever.</p>
                </div>

                <img class="main__image main__image--one" src="/assets/images/deforestation/deforestation-one.jpg" alt="Logs lying on the ground after being cut down.">

                <div class="main__column-two-text">
                  <h3 class="pre-title fs--16">Agriculture</h3>
                  <p class="bodyfont">Agriculture is the largest single cause of the planet's deforestation. Forests are regularly being destroyed in exchange for plantations for palm oil, soy and rubber. Just in Indonesia,  an area the size of a <a class="link" href="https://www.greenpeace.org.uk/what-we-do/forests/indonesia/what-is-palm-oil/" target="_blank">football pitch is lost every 25 seconds</a> to make way for palm oil plantations. Wildlife, such as orangutans, are in huge danger due to these plantations.</p>
                </div>

                <img class="main__image main__image--two" src="/assets/images/deforestation/deforestation-two.jpg" alt="Orangutan hugging baby in a forest.">

                <div class="main__end">
                  <h3 class="pre-title fs--20">What can we do?</h3>
                  <p class="bodyfont fs--18">Everyone can make a contribution to reducing deforestation. Making an effort to consume fewer wood products and paper, recycling what we do consume, and looking out for sustainably produced palm oil are all ways in which we can contribute. The future of the planet depends on it. </p>

                  <div class="main__end-buttons">
                    <a class="button button--back" href="#">Go Back</a>
                    <a class="button button--top" href="#">Top</a>
                  </div>
                </div>

              </div> <!-- container -->
            </div> <!-- page -->

          </div>
        </div>

      </li>

    </ul>

    <nav class="main__nav">
      <ul class="main__nav-list">
        <li class="main__nav-item">
          <a class="main__nav-link main__nav-link--oceans pre-title active" href="#">Oceans</a>
        </li>

        <li class="main__nav-item">
          <a class="main__nav-link main__nav-link--air pre-title" href="#">Air</a>
        </li>

        <li class="main__nav-item">
          <a class="main__nav-link main__nav-link--deforestation pre-title" href="#">Deforestation</a>
        </li>
      </ul>
    </nav>
  </section>

  <section class="about">
    <div class="about__content">
      <a class="about__close" href="">
        <?php include "assets/svgs/close.svg";?>
      </a>

      <h2 class="header fs--50 font--black">Save Our Planet</h2>
      <p class="bodyfont fs--18 font--black">This project was created to highlight the current environmental issues the planet is facing in the oceans, the air and forests. This aims to spread awareness of the issues and provide simple steps that everyone can take to contribute to the reduction of the Earth's pollution.</p>
      <p class="bodyfont fs--18 font--black">This project was created by <a class="link font--black" href="https://ashleydutton.co.uk" target="_blank">Ashley Dutton.</a></p> 
    </div>
    <a class="about__overlay" href="#"></a>
  </section>

</main>

<?php
  include "footer.php";
?>
