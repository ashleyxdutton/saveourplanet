<!DOCTYPE html>
<html lang="en">
<head>

	<!-- metadata -->
	<meta charset="UTF-8">
	<title>SaveOurPlanet - Environmental Issues</title>
  <meta name="description" content="<?php echo $description;?>">
	<meta name="keywords" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

	<!-- stylesheets -->
	<link rel="stylesheet" type="text/css" href="/assets/css/main.css">

	<!-- fonts -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,600%7CSource+Serif+Pro" rel="stylesheet">

	<!-- favicon -->
	<link rel="apple-touch-icon" sizes="57x57" href="/assets/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/assets/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/assets/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/assets/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/assets/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="/assets/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/assets/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- Google Analytics Tracking -->


</head>
<body>

<header class="site-header">

	<a class="site-header__icon site-header__icon--info" href="/">
		<?php include "assets/svgs/info.svg";?>
	</a>

	<a class="site-header__logo header" href="/">Save Our Planet</a>

	<a class="site-header__icon" href="https://gitlab.com/ashleyxdutton" target="_blank">
		<?php include "assets/svgs/gitlab.svg";?>
	</a>

</header>
