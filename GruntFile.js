// GRUNT COMMANDS
module.exports = function(grunt) {

  require('jit-grunt')(grunt);

  // CONFIGURE SETTINGS
  grunt.initConfig({

    // BASIC SETTINGS ABOUT PLUGINS
    pkg: grunt.file.readJSON('package.json'),

    // SASS PLUGINS
    sass: {
      dist: { // Target
        options: { // Target options
          style: 'compressed'
        },
        files: { // Dictionary of files
          'assets/css/main.css': 'assets/css/sass/style.scss' // 'destination': 'source'
        }
      }
    },

    // ADD PREFIXES TO MAIN.CSS AND RE-COMPRESS
    postcss: {
      options: {
        map: false,
        processors: [
          require('autoprefixer')
        ]
      },
      dist: {
        src: 'assets/css/main.css'
      }
    },

    // WATCH FILES FOR CHANGES
    watch: {
      sass: {
        files: ['assets/css/sass/**'],
        tasks: ['sass']
      },
      postcss: {
        files: ['assets/css/sass/**'],
        tasks: ['postcss']
      }
    }

  });

  // DO THE TASK
  grunt.registerTask('default', ['sass']);
  grunt.registerTask('default', ['watch']);
};
